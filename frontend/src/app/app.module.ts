import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrarComponent } from './components/registrar/registrar.component';
import { AdminComponent } from './components/admin/admin.component';
import {routing} from './app.routing';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FileSelectDirective } from 'ng2-file-upload';
import { Confignivel1Component } from './components/confignivel1/confignivel1.component';
import { Nivel1Component } from './components/nivel1/nivel1.component';
import { NivelopuestosService } from './services/nivelopuestos.service';
import { Confignivel2Component } from './components/confignivel2/confignivel2.component';
import { Nivel21Component } from './components/nivel21/nivel21.component';
import { Nivel23Component } from './components/nivel23/nivel23.component';
import { Nivel25Component } from './components/nivel25/nivel25.component';
import { Confignivel3Component } from './components/confignivel3/confignivel3.component';
import { Nivel32Component } from './components/nivel32/nivel32.component';
import { Nivel33Component } from './components/nivel33/nivel33.component';
import { Nivel34Component } from './components/nivel34/nivel34.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegistrarComponent,
    AdminComponent,
    FileSelectDirective,
    Confignivel1Component,
    Nivel1Component,
    Confignivel2Component,
    Nivel21Component,
    Nivel23Component,
    Nivel25Component,
    Confignivel3Component,
    Nivel32Component,
    Nivel33Component,
    Nivel34Component
  ],
  imports: [
    BrowserModule,
    routing, 
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
    //FileSelectDirective
  ],
  providers: [NivelopuestosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
