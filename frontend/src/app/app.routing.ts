import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from "@angular/core";
import {AdminComponent} from "./components/admin/admin.component";
import {HomeComponent} from "./components/home/home.component";
import {LoginComponent} from "./components/login/login.component";
import {RegistrarComponent} from "./components/registrar/registrar.component";
import {Confignivel1Component} from "./components/confignivel1/confignivel1.component";
import {Confignivel2Component} from "./components/confignivel2/confignivel2.component";
import {Confignivel3Component} from "./components/confignivel3/confignivel3.component";
import {Nivel1Component} from "./components/nivel1/nivel1.component";
import {Nivel21Component} from "./components/nivel21/nivel21.component";
import {Nivel23Component} from "./components/nivel23/nivel23.component";
import {Nivel25Component} from "./components/nivel25/nivel25.component";
import {Nivel32Component} from "./components/nivel32/nivel32.component";
import {Nivel33Component} from "./components/nivel33/nivel33.component";
import {Nivel34Component} from "./components/nivel34/nivel34.component";



const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'registrar', component: RegistrarComponent },
  { path: 'admin/:id', component: AdminComponent },
  { path: 'confign1', component: Confignivel1Component},
  { path: 'confign2', component: Confignivel2Component},
  { path: 'confign3', component: Confignivel3Component},
  { path: 'nivel1', component: Nivel1Component},
  { path: 'nivel21', component: Nivel21Component},
  { path: 'nivel23', component: Nivel23Component},
  { path: 'nivel25', component: Nivel25Component},
  { path: 'nivel32', component: Nivel32Component},
  { path: 'nivel33', component: Nivel33Component},
  { path: 'nivel34', component: Nivel34Component},
  {path : '', redirectTo: '/home', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
