import { Component, OnInit } from '@angular/core';
import { FileUploader, FileSelectDirective, FileItem } from 'ng2-file-upload/ng2-file-upload'
import { UsuarioService } from '../../services/usuario.service';
import { ActivatedRoute } from '@angular/router'
import { OpuestoService } from '../../services/opuesto.service';
import {Opuesto} from '../../models/opuesto';
import { NgForm } from '@angular/forms';
import { Usuario } from '../../models/usuario';
const URL = 'http://localhost:3000/upload';

declare var M: any;

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  providers: [UsuarioService, OpuestoService]
})
export class AdminComponent implements OnInit {
  imageSrc: string;
  imageSrc1: string;
  id: string;
  savedOpuesto: Opuesto;
  test: Opuesto;
  obj: any;
  Usr: Usuario;
  json: JSON;
  opId: string;

  public uploader: FileUploader = new FileUploader({url: URL, itemAlias: 'photo', allowedMimeType: ['image/png', 'image/jpeg', 'image/bmp', 'image/gif', 'image/jpg']});
  public uploader1: FileUploader = new FileUploader({url: URL, itemAlias: 'photo'});

  constructor(private route: ActivatedRoute, private opuestoService: OpuestoService, private usuarioService: UsuarioService) { }

  ngOnInit() {
    
    //override the onAfterAddingfile property of the uploader so it doesn't authenticate with //credentials.
    this.uploader.onAfterAddingFile = (file)=> { file.withCredentials = false; };
    this.uploader1.onAfterAddingFile = (file)=> { file.withCredentials = false; };
    //overide the onCompleteItem property of the uploader so we are 
    //able to deal with the server response.
   /* this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
        //console.log("ImageUpload: uploaded:", item, status, response);
        console.log(response);
        var lol = response;
        console.log(lol);
    };*/
    this.route.params.subscribe(params => {
      this.id = params['id'];
      console.log(this.id);
    });
   
    (<HTMLInputElement>document.getElementById('idUsuario')).value = this.id;
    console.log((<HTMLInputElement>document.getElementById('idUsuario')).value);
    
    
    //this.getUsuario();
    this.getOpuestosUsuario();
    (<HTMLInputElement>document.getElementById('btn1')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('btn2')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('btn3')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('opuesto1')).disabled = true;
    (<HTMLInputElement>document.getElementById('opuesto2')).disabled = true;
    (<HTMLInputElement>document.getElementById('opuesto_im1')).disabled = true;
    (<HTMLInputElement>document.getElementById('opuesto_im2')).disabled = true;
    (<HTMLInputElement>document.getElementById('div1')).style.background = 'grey';
    (<HTMLInputElement>document.getElementById('div2')).style.background = 'grey';
    (<HTMLInputElement>document.getElementById('loader')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('msjagregar')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('msjeditar')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('msjborrar')).style.display = 'none';

  }


  upload(form: NgForm){
    var cont = 0;
    var ip1;
    var ip2;
    var opuesto1 = (<HTMLInputElement>document.getElementById('opuesto1')).value;
    var opuesto2 = (<HTMLInputElement>document.getElementById('opuesto2')).value;
    var nombre_im1 = '';
    var nombre_im2 = '';
    var opuesto_im1 = (<HTMLInputElement>document.getElementById('opuesto_im1')).value;
    var opuesto_im2 = (<HTMLInputElement>document.getElementById('opuesto_im2')).value;

    this.getUsuario();
    console.log(this.usuarioService.usuarios);
    

    if(opuesto1 == ''){
      M.toast({html: 'Ingrese todos los datos de opuestos'});
    }
    else if(opuesto2 == ''){
      M.toast({html: 'Ingrese todos los datos de opuestos'});
    }else if(opuesto_im1 == ''){
      M.toast({html: 'Ingrese todos los datos de opuestos'});
    }else if(opuesto_im2 == ''){
       M.toast({html: 'Ingrese todos los datos de opuestos'});
    } else {
      (<HTMLInputElement>document.getElementById('loader')).style.display = 'block';
      (<HTMLInputElement>document.getElementById('form')).style.display = 'none';
      this.uploader.uploadAll();
      this.uploader.onCompleteItem = (item:FileItem, response:any, status:any, headers:any) => {
        //console.log("ImageUpload: uploaded:", item, status, response);
        if(cont == 0){
          console.log(" response: " + JSON.parse(response));
          //this.json = <JSON>response;
          ip1 = JSON.parse(response).url;
          nombre_im1 = JSON.parse(response).file;
          console.log('ip1: ' + ip1);
          console.log('nombre1: ' + nombre_im1)
          cont++;
          console.log(cont);
        } else if(cont == 1) {
          console.log(" response: " + response);
          ip2 = JSON.parse(response).url;
          nombre_im2 = JSON.parse(response).file;
          console.log('ip2: ' + ip2);
          console.log('nombre2: ' + nombre_im2)
          cont++;
          console.log(cont);
       
        } 
        if(cont == 2){
          this.imageSrc = '';
          this.imageSrc1 = '';;
          this.obj = {
            "url_im1": ip1,
            "url_im2": ip2,
            "nombre_im1": nombre_im1,
            "nombre_im2": nombre_im2,
            "opuesto_im1": (<HTMLInputElement>document.getElementById('opuesto_im1')).value.toLowerCase(),
            "opuesto_im2": (<HTMLInputElement>document.getElementById('opuesto_im2')).value.toLowerCase(),
            "usuario": this.id
          };
          this.test = <Opuesto>this.obj;
          console.log(this.test);
          this.opuestoService.postOpuesto(this.test).subscribe(res => {
            console.log(222);
          });
          (<HTMLInputElement>document.getElementById('opuesto1')).value = '';
          (<HTMLInputElement>document.getElementById('opuesto2')).value = '';
          (<HTMLInputElement>document.getElementById('opuesto_im1')).value = '';
          (<HTMLInputElement>document.getElementById('opuesto_im2')).value = '';
          (<HTMLInputElement>document.getElementById('wrapper1')).value = '';
          (<HTMLInputElement>document.getElementById('wrapper2')).value = '';
          (<HTMLInputElement>document.getElementById('loader')).style.display = 'none';
          (<HTMLInputElement>document.getElementById('form')).style.display = 'block';
          M.toast({html: 'Opuestos Cargados'}); 
          cont++;
          console.log(cont);
        }
        if(cont == 3){
          this.getOpuestosUsuario();
          location.reload();
        }
      };
    }
  }

  readUrl(event: any){
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = e => this.imageSrc = reader.result as string;

      reader.readAsDataURL(file);
  }
}

readUrl1(event: any){
  if (event.target.files && event.target.files[0]) {
    const file = event.target.files[0];

    const reader = new FileReader();
    reader.onload = e => this.imageSrc1 = reader.result as string;

    reader.readAsDataURL(file);
  }
}

getUsuario(){
  this.usuarioService.getUsuario(this.id).subscribe(res =>{
    this.usuarioService.usuarios = res as Usuario[];
    console.log(res);
    //this.Usr = this.usuarioService.usuarios
    //this.Usr = this.usuarioService.usuarios as Usuario[];
  })
}

getOpuestosUsuario(){
  this.usuarioService.getOpuestosUsuario(this.id).subscribe(res => {
    this.usuarioService.opuestos = res as Opuesto[];
    //console.log("Opuestos Usuarios: " + JSON.stringify(res, null, 4));
    console.log(res);
  })
}

getOpuesto(id: string){
  this.opuestoService.getOpuesto(id).subscribe(res => {
    this.opuestoService.opuesto = res as Opuesto;
    this.savedOpuesto = this.opuestoService.opuesto;
    console.log(res);
    //console.log(this.opuestoService.opuesto);
  })
}

selectOpuesto(opuesto: Opuesto){
  this.opuestoService.selectedOpuesto = opuesto;
  this.imageSrc = this.opuestoService.selectedOpuesto.url_im1;
  this.imageSrc1 = this.opuestoService.selectedOpuesto.url_im2;
  (<HTMLInputElement>document.getElementById('opuesto_im1')).value = this.opuestoService.selectedOpuesto.opuesto_im1;
  (<HTMLInputElement>document.getElementById('opuesto_im2')).value = this.opuestoService.selectedOpuesto.opuesto_im2;
  (<HTMLInputElement>document.getElementById('idOpuesto')).value = this.opuestoService.selectedOpuesto._id;
  console.log('id Opuesto: ' + (<HTMLInputElement>document.getElementById('idOpuesto')).value);
  this.savedOpuesto = this.opuestoService.selectedOpuesto;
  console.log("saved opuesto: " + this.savedOpuesto.nombre_im1);
  console.log("selected Opuesto: " + this.opuestoService.selectedOpuesto);

}

deleteOpuesto(){
  var id = (<HTMLInputElement>document.getElementById('idOpuesto')).value;
  if(!id){
    alert('Debe seleccionar una pareja de opuestos para borrar')
  } else if(confirm('Esta seguro de elimnar estos Opuestos?')){
    this.opuestoService.deleteOpuesto(id).subscribe(res => {
      this.getOpuestosUsuario();
      M.toast({html: 'Opuestos Borrados!'});
      (<HTMLInputElement>document.getElementById('opuesto1')).value = '';
      (<HTMLInputElement>document.getElementById('opuesto2')).value = '';
      (<HTMLInputElement>document.getElementById('opuesto_im1')).value = '';
      (<HTMLInputElement>document.getElementById('opuesto_im2')).value = '';
      (<HTMLInputElement>document.getElementById('idOpuesto')).value = '';
      (<HTMLInputElement>document.getElementById('wrapper1')).value = '';
      (<HTMLInputElement>document.getElementById('wrapper2')).value = '';
      this.imageSrc = '';
      this.imageSrc1 = '';
    });
  }
}

editOpuesto(){
  var id = (<HTMLInputElement>document.getElementById('idOpuesto')).value;
  console.log(id);
  //this.getOpuesto(id);
  var getop = this.savedOpuesto;
  //console.log("getop: " + getop.nombre_im1);
  var urop1 = (<HTMLInputElement>document.getElementById('opuesto1')).value;
  var urop2 = (<HTMLInputElement>document.getElementById('opuesto2')).value;
  var n1;
  var n2;
  var aux1 = true;
  var aux2 = true;
  var cont = 0;
  var cont2 = 0;

  if(!id){
    alert('Debe seleccionar una pareja de opuestos para borrar')
  }else if(urop1 != '' && urop2 != ''){
    console.log('se modificaron ambas imagenes');
    this.uploader.uploadAll();
    this.uploader.onCompleteItem = (item:FileItem, response:any, status:any, headers:any) => {
      if(cont2==0){
        console.log(" response: " + JSON.parse(response));
        //this.json = <JSON>response;
        urop1 = JSON.parse(response).url;
        n1 = JSON.parse(response).file;
        console.log('ip1: ' + urop1);
        console.log('nombre1: ' + n1)
        cont2++;
        console.log("se modifico im1 " + cont2);
        aux1 = false;
      }else if(cont2==1){
        console.log(" response: " + JSON.parse(response));
          //this.json = <JSON>response;
          urop2 = JSON.parse(response).url;
          n2 = JSON.parse(response).file;
          console.log('ip2: ' + urop2);
          console.log('nombre2: ' + n2)
          cont2++;
          console.log("se modifico im2 " + cont2);
          aux2 = false;
      }
      if(cont2 >=2){
        console.log("guardar datos foto alterada");
        this.obj = {
          "_id": id,
          "url_im1": urop1,
          "url_im2": urop2,
          "nombre_im1": n1,
          "nombre_im2": n2,
          "opuesto_im1": (<HTMLInputElement>document.getElementById('opuesto_im1')).value.toLowerCase(),
          "opuesto_im2": (<HTMLInputElement>document.getElementById('opuesto_im2')).value.toLowerCase()
        };
        this.test = <Opuesto>this.obj;
        console.log(this.test);
        this.opuestoService.putOpuesto(this.test).subscribe(res => {
          console.log("Editado correctamente");
          this.getOpuestosUsuario();
          M.toast({html: 'Editado correctamente!'});
          (<HTMLInputElement>document.getElementById('opuesto1')).value = '';
          (<HTMLInputElement>document.getElementById('opuesto2')).value = '';
          (<HTMLInputElement>document.getElementById('opuesto_im1')).value = '';
          (<HTMLInputElement>document.getElementById('opuesto_im2')).value = '';
          (<HTMLInputElement>document.getElementById('idOpuesto')).value = '';
          (<HTMLInputElement>document.getElementById('wrapper1')).value = '';
          (<HTMLInputElement>document.getElementById('wrapper2')).value = '';
          this.imageSrc = '';
          this.imageSrc1 = '';
          cont2 = 0;
          this.disableop1();
          this.disableop2();
          this.disableEditar();
        });
      }
    }

  } else if(urop1 != '' || urop2 != ''){
    console.log("se modifico alguna de las imagenes");
    if(urop1 == ''){
      urop1 = getop.url_im1;
      n1 = getop.nombre_im1;
      aux1 = false;
      console.log("no se modifico imagen 1");
    }
    if(urop2 == ''){
      urop2 = getop.url_im2;
      n2 = getop.nombre_im2;
      aux2 = false;
      console.log("no se modifico imagen 2");
    }

    this.uploader.uploadAll();
      this.uploader.onCompleteItem = (item:FileItem, response:any, status:any, headers:any) => {
        if(aux1 && !aux2){
          console.log(" response: " + JSON.parse(response));
          //this.json = <JSON>response;
          urop1 = JSON.parse(response).url;
          n1 = JSON.parse(response).file;
          console.log('ip1: ' + urop1);
          console.log('nombre1: ' + n1)
          cont++;
          console.log("se modifico im1 " + cont);
          aux1 = false;
        } 
        if(aux2 && !aux1) {
          console.log(" response: " + JSON.parse(response));
          //this.json = <JSON>response;
          urop2 = JSON.parse(response).url;
          n2 = JSON.parse(response).file;
          console.log('ip2: ' + urop2);
          console.log('nombre2: ' + n2)
          cont++;
          console.log("se modifico im2 " + cont);
          aux2 = false;
        }
        if(cont >= 1){
          console.log("guardar datos foto alterada");
          this.obj = {
            "_id": id,
            "url_im1": urop1,
            "url_im2": urop2,
            "nombre_im1": n1,
            "nombre_im2": n2,
            "opuesto_im1": (<HTMLInputElement>document.getElementById('opuesto_im1')).value.toLowerCase(),
            "opuesto_im2": (<HTMLInputElement>document.getElementById('opuesto_im2')).value.toLowerCase()
          };
          this.test = <Opuesto>this.obj;
          console.log(this.test);
          this.opuestoService.putOpuesto(this.test).subscribe(res => {
            console.log("Editado correctamente");
            this.getOpuestosUsuario();
            M.toast({html: 'Editado correctamente!'});
            (<HTMLInputElement>document.getElementById('opuesto1')).value = '';
            (<HTMLInputElement>document.getElementById('opuesto2')).value = '';
            (<HTMLInputElement>document.getElementById('opuesto_im1')).value = '';
            (<HTMLInputElement>document.getElementById('opuesto_im2')).value = '';
            (<HTMLInputElement>document.getElementById('idOpuesto')).value = '';
            (<HTMLInputElement>document.getElementById('wrapper1')).value = '';
            (<HTMLInputElement>document.getElementById('wrapper2')).value = '';
            this.imageSrc = '';
            this.imageSrc1 = '';
            cont =0;
            this.disableop1();
            this.disableop2();
            this.disableEditar();
          });
        }
      }
    } else {
      console.log("NO se modifico alguna de las imagenes");
      console.log(getop);
      urop1 = getop.url_im1;
      urop2 = getop.url_im2;
      n1 = getop.nombre_im1;
      n2 = getop.nombre_im2;
      this.obj = {
        "_id": id,
        "url_im1": urop1,
        "url_im2": urop2,
        "nombre_im1": n1,
        "nombre_im2": n2,
        "opuesto_im1": (<HTMLInputElement>document.getElementById('opuesto_im1')).value.toLowerCase(),
        "opuesto_im2": (<HTMLInputElement>document.getElementById('opuesto_im2')).value.toLowerCase()
      };
      this.test = <Opuesto>this.obj;
      console.log(this.test);
      this.opuestoService.putOpuesto(this.test).subscribe(res => {
        console.log("Editado correctamente");
        this.getOpuestosUsuario();
        M.toast({html: 'Editado correctamente!'});
        (<HTMLInputElement>document.getElementById('opuesto1')).value = '';
        (<HTMLInputElement>document.getElementById('opuesto2')).value = '';
        (<HTMLInputElement>document.getElementById('opuesto_im1')).value = '';
        (<HTMLInputElement>document.getElementById('opuesto_im2')).value = '';
        (<HTMLInputElement>document.getElementById('idOpuesto')).value = '';
        (<HTMLInputElement>document.getElementById('wrapper1')).value = '';
        (<HTMLInputElement>document.getElementById('wrapper2')).value = '';
        this.imageSrc = '';
        this.imageSrc1 = '';
        this.disableop1();
        this.disableop2();
        this.disableEditar();
      });
    }
}

enableAgreagar(){
  (<HTMLInputElement>document.getElementById('btn1')).style.display = 'block';
  (<HTMLInputElement>document.getElementById('btn2')).style.display = 'none';
  (<HTMLInputElement>document.getElementById('btn3')).style.display = 'none';
  (<HTMLInputElement>document.getElementById('opuesto1')).disabled = false;
  (<HTMLInputElement>document.getElementById('opuesto2')).disabled = false;
  (<HTMLInputElement>document.getElementById('opuesto_im1')).disabled = false;
  (<HTMLInputElement>document.getElementById('opuesto_im2')).disabled = false;
  (<HTMLInputElement>document.getElementById('div1')).style.background = 'dodgerblue';
  (<HTMLInputElement>document.getElementById('div2')).style.background = 'dodgerblue'; 
  (<HTMLInputElement>document.getElementById('opuesto1')).style.display = 'block';
  (<HTMLInputElement>document.getElementById('opuesto2')).style.display = 'block';
      (<HTMLInputElement>document.getElementById('msjagregar')).style.display = 'block';
    (<HTMLInputElement>document.getElementById('msjeditar')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('msjborrar')).style.display = 'none';


  (<HTMLInputElement>document.getElementById('opuesto1')).value = '';
  (<HTMLInputElement>document.getElementById('opuesto2')).value = '';
  (<HTMLInputElement>document.getElementById('opuesto_im1')).value = '';
  (<HTMLInputElement>document.getElementById('opuesto_im2')).value = ''; 
  (<HTMLInputElement>document.getElementById('wrapper1')).value = '';
  (<HTMLInputElement>document.getElementById('wrapper2')).value = ''; 
  this.imageSrc = '';
  this.imageSrc1 = '';
  this.getOpuestosUsuario();
  this.uploader.clearQueue();
}
disableAgregar(){
  (<HTMLInputElement>document.getElementById('btn1')).style.display = 'none';
}
enableBorrar(){
  (<HTMLInputElement>document.getElementById('btn2')).style.display = 'block';
  (<HTMLInputElement>document.getElementById('btn1')).style.display = 'none';
  (<HTMLInputElement>document.getElementById('btn3')).style.display = 'none';
  (<HTMLInputElement>document.getElementById('opuesto1')).disabled = true;
  (<HTMLInputElement>document.getElementById('opuesto2')).disabled = true;
  (<HTMLInputElement>document.getElementById('opuesto_im1')).disabled = true;
  (<HTMLInputElement>document.getElementById('opuesto_im2')).disabled = true; 
  (<HTMLInputElement>document.getElementById('div1')).style.background = 'grey';
  (<HTMLInputElement>document.getElementById('div2')).style.background = 'grey';
      (<HTMLInputElement>document.getElementById('msjagregar')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('msjeditar')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('msjborrar')).style.display = 'block';
}
disableBorrar(){
  (<HTMLInputElement>document.getElementById('btn2')).style.display = 'none';
}
enableEditar(){
  (<HTMLInputElement>document.getElementById('btn3')).style.display = 'block';
  (<HTMLInputElement>document.getElementById('btn1')).style.display = 'none';
  (<HTMLInputElement>document.getElementById('btn2')).style.display = 'none';
  (<HTMLInputElement>document.getElementById('opuesto1')).disabled = false;
  (<HTMLInputElement>document.getElementById('opuesto2')).disabled = false;
  (<HTMLInputElement>document.getElementById('opuesto_im1')).disabled = false;
  (<HTMLInputElement>document.getElementById('opuesto_im2')).disabled = false;
  (<HTMLInputElement>document.getElementById('div1')).style.background = 'dodgerblue';
  (<HTMLInputElement>document.getElementById('div2')).style.background = 'dodgerblue'; 
  (<HTMLInputElement>document.getElementById('opuesto1')).style.display = 'block';
  (<HTMLInputElement>document.getElementById('opuesto2')).style.display = 'block';
      (<HTMLInputElement>document.getElementById('msjagregar')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('msjeditar')).style.display = 'block';
    (<HTMLInputElement>document.getElementById('msjborrar')).style.display = 'none';


  this.uploader.clearQueue(); 
}
disableEditar(){
  (<HTMLInputElement>document.getElementById('btn3')).style.display = 'none';
}

disableop1(){
  (<HTMLInputElement>document.getElementById('opuesto1')).style.display = 'none';
  (<HTMLInputElement>document.getElementById('div1')).style.background = 'grey';

}

disableop2(){
  (<HTMLInputElement>document.getElementById('opuesto2')).style.display = 'none';
  (<HTMLInputElement>document.getElementById('div2')).style.background = 'grey';

}

}
