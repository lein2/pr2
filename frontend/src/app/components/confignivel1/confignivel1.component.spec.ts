import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Confignivel1Component } from './confignivel1.component';

describe('Confignivel1Component', () => {
  let component: Confignivel1Component;
  let fixture: ComponentFixture<Confignivel1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Confignivel1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Confignivel1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
