import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Confignivel2Component } from './confignivel2.component';

describe('Confignivel2Component', () => {
  let component: Confignivel2Component;
  let fixture: ComponentFixture<Confignivel2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Confignivel2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Confignivel2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
