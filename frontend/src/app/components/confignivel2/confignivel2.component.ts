import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { OpuestoService } from '../../services/opuesto.service';
import {Opuesto} from '../../models/opuesto';
import { NgForm } from '@angular/forms';
import { Usuario } from '../../models/usuario';

declare var M: any;


@Component({
  selector: 'app-confignivel2',
  templateUrl: './confignivel2.component.html',
  styleUrls: ['./confignivel2.component.css'],
  providers: [UsuarioService, OpuestoService]
})
export class Confignivel2Component implements OnInit {

  imageSrc: string;
  imageSrc1: string;
  id: string;
  idUsuario: string;
  savedOpuesto: Opuesto;
  usuarios: Usuario[];
  opuestos: Opuesto[];
  opuestosJuego: Array<Opuesto> = [];
  usuariosLenght: number;
  opuestosJuego2: Array<Opuesto>;

  constructor(private usuarioService: UsuarioService, private opuestoService: OpuestoService) { }

  ngOnInit() {
    this.getUsuarios();
    this.getOpuestos();
    //console.log(this.usuarios);
    //console.log(this.usuarioService.usuarios);
    //this.fillSelect();
    console.log(this.opuestosJuego.length);
  }

  async getUsuarios(){
    var select1 = (<HTMLInputElement>document.getElementById('elegirUsuario'));
    this.usuarioService.getUsuarios().subscribe(res =>{
      this.usuarioService.usuarios = res as Usuario[];
      console.log(res);
    });
  }

  getOpuestos(){
    this.opuestoService.getOpuestos().subscribe(res => {
      this.opuestoService.opuestos = res as Opuesto[];
      this.opuestos = this.opuestoService.opuestos;
    })
  }

  getOpuestosUsuario(id: string){
    this.usuarioService.getOpuestosUsuario(id).subscribe(res => {
      this.usuarioService.opuestos = res as Opuesto[];
      //console.log("Opuestos Usuarios: " + JSON.stringify(res, null, 4));
      console.log(res);
      this.usuarioService.opuestos;
    
    })
  }

  selectUsuario(usuario: Usuario){
    this.usuarioService.selectedUsuario = usuario;
    this.idUsuario = this.usuarioService.selectedUsuario._id;
    this.imageSrc = '';
    this.imageSrc1 = '';
    (<HTMLInputElement>document.getElementById('idUsuario')).value = this.idUsuario;
    console.log('idUsuario:' + (<HTMLInputElement>document.getElementById('idUsuario')).value);
    this.getOpuestosUsuario(this.idUsuario);
  
  }

  selectOpuesto(opuesto: Opuesto){
    this.opuestoService.selectedOpuesto = opuesto;
    this.imageSrc = this.opuestoService.selectedOpuesto.url_im1;
    this.imageSrc1 = this.opuestoService.selectedOpuesto.url_im2;
    (<HTMLInputElement>document.getElementById('opuesto_im1')).value = this.opuestoService.selectedOpuesto.opuesto_im1;
    (<HTMLInputElement>document.getElementById('opuesto_im2')).value = this.opuestoService.selectedOpuesto.opuesto_im2;
    //(<HTMLInputElement>document.getElementById('idOpuesto')).value = this.opuestoService.selectedOpuesto._id;
    //console.log('id Opuesto: ' + (<HTMLInputElement>document.getElementById('idOpuesto')).value);
    this.savedOpuesto = this.opuestoService.selectedOpuesto;
    console.log("saved opuesto: " + this.savedOpuesto.nombre_im1);
    console.log("selected Opuesto: " + this.opuestoService.selectedOpuesto);
  
  }

  async fillSelect(){
    await this.getUsuarios();
    var select1 = (<HTMLSelectElement>document.getElementById('elegirUsuario'));
    var usuarios = this.usuarioService.usuarios;
    console.log(this.usuarios);
    console.log(usuarios.length);
    for(var i = 0; i < usuarios.length; i++){
      var user = usuarios[i];
      console.log(user);
      var option = document.createElement('option');
      option.textContent = user.username;
      option.value = user._id;
      select1.appendChild(option);
    }
  }

  pushOpuesto(opuesto: Opuesto){
    this.opuestoService.selectedOpuesto = opuesto;
    //console.log(opuesto);
    var repetido = this.opuestosJuego.find(function(opuestoJuego){
      return opuestoJuego === opuesto;
    });

    if(repetido){
      M.toast({html: 'Ya se escogio este opuesto'});
    } else {
    this.opuestosJuego.push(opuesto);
    console.log(this.opuestosJuego);
    }
  }

  spliceOpuesto(opuesto: Opuesto){
    this.opuestoService.selectedOpuesto = opuesto;
    this.opuestosJuego.splice(this.opuestosJuego.indexOf(opuesto), 1);
    console.log(this.opuestosJuego);
  }

  selectValue(){
    var select1 = (<HTMLSelectElement>document.getElementById('elegirUsuario'));
    (<HTMLInputElement>document.getElementById('idUsuario')).value = select1.options[select1.selectedIndex].value;
    this.idUsuario = (<HTMLInputElement>document.getElementById('idUsuario')).value;
    console.log(this.idUsuario);
    if(this.idUsuario == "-1"){
      M.toast({html: 'Seleccione un usuario'});
    } else{
      this.getOpuestosUsuario(this.idUsuario);
    }
  }

  startnivel(){
    if(this.opuestosJuego.length == 0){
      M.toast({html: 'Debe seleccionar al menos 1 opuesto para el nivel'});
    } else {
      var select2 = (<HTMLSelectElement>document.getElementById('numDistractores'));
      localStorage.setItem('opuestos', JSON.stringify(this.opuestosJuego));
      this.getUniqueOpuestos(this.opuestosJuego);
      localStorage.setItem('opuestosfull', JSON.stringify(this.opuestos));
      if(select2.options[select2.selectedIndex].value == "1"){
        var x = location.href='/nivel21';
        document.getElementById('boton').innerHTML = x;
      } else if(select2.options[select2.selectedIndex].value == "3"){
        var x = location.href='/nivel23';
        document.getElementById('boton').innerHTML = x;
      } else if(select2.options[select2.selectedIndex].value == "5"){
        var x = location.href='/nivel25';
        document.getElementById('boton').innerHTML = x;
      } else {
        M.toast({html: 'Debe seleccionar el número de deistractores'});
      }
    }
  }

  getOpuestosjuego(){
    //console.log(this.opuestosJuego);
    return this.opuestosJuego2;
  }

  getUniqueOpuestos(opuestos: Array<Opuesto>){
    for(var i = 0; i<this.opuestos.length; i++){
      for(var j = 0; j<opuestos.length; j++){
        if(this.opuestos[i]._id == opuestos[j]._id){
          this.opuestos.splice(i,1);
        }
      }
    }
  }

}
