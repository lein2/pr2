import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Confignivel3Component } from './confignivel3.component';

describe('Confignivel3Component', () => {
  let component: Confignivel3Component;
  let fixture: ComponentFixture<Confignivel3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Confignivel3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Confignivel3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
