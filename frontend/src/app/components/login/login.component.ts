import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsuarioService } from '../../services/usuario.service';
import {Usuario} from '../../models/usuario';

declare var M: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UsuarioService]
})
export class LoginComponent implements OnInit {

  constructor(private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.getUsuarios();
  }

  loginUsuario(form: NgForm){
    this.getUsuarios();
    var user = (<HTMLInputElement>document.getElementById('username')).value;
    var pass = (<HTMLInputElement>document.getElementById('password')).value;
    var Usuarios = this.usuarioService.usuarios;
    
    
    var findUser = Usuarios.find(function(Usuario){
      return Usuario.username === user;
    })

    var chkPass = Usuarios.find(function(Usuario){
      return Usuario.password === pass;
    })

    if(!findUser){
      M.toast({html: 'Usuario no existente'});
    } else if(!chkPass){
      M.toast({html: 'Constraseña Incorrecta'});
    } else {
      M.toast({html: 'Usuario Correcto'});
      this.resetForm();
      var ruta = location.href = '/admin/'+ findUser._id.toString();
      document.getElementById('boton').innerHTML= ruta;
    }
    
  }

  getUsuarios(){
    this.usuarioService.getUsuarios().subscribe(res =>{
      this.usuarioService.usuarios = res as Usuario[];
      //console.log(res);
    })
  }

  resetForm(form?: NgForm){
    if(form){
      form.reset;
      this.usuarioService.selectedUsuario = new Usuario();
    }
  }

  redirectLogin(){
    var x = location.href='/login';
    document.getElementById('boton').innerHTML = x;
  }

}
