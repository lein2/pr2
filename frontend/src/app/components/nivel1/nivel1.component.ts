import { Component, OnInit, Input } from '@angular/core';
import { OpuestoService } from '../../services/opuesto.service';
import {Opuesto} from '../../models/opuesto';
import { NivelopuestosService } from '../../services/nivelopuestos.service';

@Component({
  selector: 'app-nivel1',
  templateUrl: './nivel1.component.html',
  styleUrls: ['./nivel1.component.css'],
  providers: [OpuestoService]
})
export class Nivel1Component implements OnInit {

  opuestosJuego: Array<Opuesto>;
  imageSrc: string;
  imageSrc1: string;

  constructor(private opuestoService: OpuestoService, private nivelOpuestosService: NivelopuestosService) { }
  ngOnInit() {
    (<HTMLInputElement>document.getElementById('last')).style.display = 'none';
    this.opuestosJuego = JSON.parse(localStorage.getItem('opuestos'));
    console.log(this.opuestosJuego);
    //localStorage.clear();

    this.imageSrc = this.opuestosJuego[0].url_im1;
    this.imageSrc1 = this.opuestosJuego[0].url_im2;
    (<HTMLInputElement>document.getElementById('op1')).textContent = this.opuestosJuego[0].opuesto_im1;
    (<HTMLInputElement>document.getElementById('op2')).textContent = this.opuestosJuego[0].opuesto_im2;

  }

  escucharImagen1(){
    var h = new SpeechSynthesisUtterance();
    h.lang ="es-419";
    h.text = (<HTMLInputElement>document.getElementById('op1')).textContent;
    window.speechSynthesis.speak(h);
  }

  escucharImagen2(){
    var h = new SpeechSynthesisUtterance();
    h.lang ="es-419";
    h.text = (<HTMLInputElement>document.getElementById('op2')).textContent;
    window.speechSynthesis.speak(h);
  }
  
  keyevent1(event){
    if (event.keyCode === 32) {
     console.log(event);
     this.escucharImagen1();
    }
  }
  
    keyevent2(event){
    if (event.keyCode === 32) {
     console.log(event);
     this.escucharImagen2();
    }
  }

  continuar(){
    if(this.opuestosJuego.length == 1){
      var audio = new Audio('../../../assets/img/good.mp3');
      audio.play();
      this.opuestosJuego.shift();
      (<HTMLInputElement>document.getElementById('card')).style.display = 'none';
      (<HTMLInputElement>document.getElementById('last')).style.display = 'block';
      setTimeout(function(){
        localStorage.clear();
        var x = location.href='/home';
        document.getElementById('boton').innerHTML = x;
      }, 3000);

      //alert("Se acabo");
    } else {
      this.opuestosJuego.shift();
      this.imageSrc = this.opuestosJuego[0].url_im1;
      this.imageSrc1 = this.opuestosJuego[0].url_im2;
      (<HTMLInputElement>document.getElementById('op1')).textContent = this.opuestosJuego[0].opuesto_im1;
      (<HTMLInputElement>document.getElementById('op2')).textContent = this.opuestosJuego[0].opuesto_im2;
    }
  }

  test(){
    window.speechSynthesis.speak(new SpeechSynthesisUtterance('lol'));
  }

  test2(){
    var lol;
    lol = JSON.parse(localStorage.getItem('opuestos'));
    console.log(lol)
  }

}
