import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Nivel23Component } from './nivel23.component';

describe('Nivel23Component', () => {
  let component: Nivel23Component;
  let fixture: ComponentFixture<Nivel23Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Nivel23Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Nivel23Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
