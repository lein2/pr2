import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Nivel25Component } from './nivel25.component';

describe('Nivel25Component', () => {
  let component: Nivel25Component;
  let fixture: ComponentFixture<Nivel25Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Nivel25Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Nivel25Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
