import { Component, OnInit } from '@angular/core';
import {Opuesto} from '../../models/opuesto';

@Component({
  selector: 'app-nivel25',
  templateUrl: './nivel25.component.html',
  styleUrls: ['./nivel25.component.css']
})
export class Nivel25Component implements OnInit {

  opuestosJuego: Array<Opuesto>;
  opuestos: Array<Opuesto>;
  aux: number;
  imageSrc: string;
  imageSrc1: string;
  imageSrc2: string;
  imageSrc3: string;
  imageSrc4: string;
  imageSrc5: string;
  imageSrc6: string;

  constructor() { }

  ngOnInit() {
    (<HTMLInputElement>document.getElementById('last')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
    this.opuestosJuego = JSON.parse(localStorage.getItem('opuestos'));
    this.opuestos = JSON.parse(localStorage.getItem('opuestosfull'));
    console.log(this.opuestosJuego);
    console.log(this.opuestos);

    this.inicioIm();
  }

  funcionMostrar() {
    var x = document.getElementById("derecho");
    x.style.display = "inline-block";
    x.style.width = "50%";
    x.style.height = "100%";
    var y = document.getElementById("boton");
    y.style.display = "none";
  } 

  inicioIm(){
    this.imageSrc = this.opuestosJuego[0].url_im1;
    (<HTMLInputElement>document.getElementById('op0')).textContent = this.opuestosJuego[0].opuesto_im1;
    this.aux = this.randomIntFromInterval(1,6);
    var aux2 = this.randomIntFromInterval(0,this.opuestos.length-1);
    var aux3 = this.randomIntFromInterval(0,this.opuestos.length-1);
    var aux4 = this.randomIntFromInterval(0,this.opuestos.length-1);
    var aux5 = this.randomIntFromInterval(0,this.opuestos.length-1);
    var aux6 = this.randomIntFromInterval(0,this.opuestos.length-1);

    console.log("aux: " + this.aux);
    console.log(aux2 + ' '+ aux3 + ' ' + aux4 + ' ' + aux5 + ' ' + aux6);

    if(this.aux == 1){
      this.imageSrc1 = this.opuestosJuego[0].url_im2;
      (<HTMLInputElement>document.getElementById('op1')).textContent = this.opuestosJuego[0].opuesto_im2;

      this.imageSrc2 = this.opuestos[aux2].url_im1;
      (<HTMLInputElement>document.getElementById('op2')).textContent = this.opuestos[aux2].opuesto_im1;

      this.imageSrc3 = this.opuestos[aux3].url_im2;
      (<HTMLInputElement>document.getElementById('op3')).textContent = this.opuestos[aux3].opuesto_im2;

      this.imageSrc4 = this.opuestos[aux4].url_im1;
      (<HTMLInputElement>document.getElementById('op4')).textContent = this.opuestos[aux4].opuesto_im1;

      this.imageSrc5 = this.opuestos[aux5].url_im1;
      (<HTMLInputElement>document.getElementById('op5')).textContent = this.opuestos[aux5].opuesto_im1;

      this.imageSrc6 = this.opuestos[aux6].url_im2;
      (<HTMLInputElement>document.getElementById('op6')).textContent = this.opuestos[aux6].opuesto_im2;

    } else if(this.aux == 2){
      this.imageSrc2 = this.opuestosJuego[0].url_im2;
      (<HTMLInputElement>document.getElementById('op2')).textContent = this.opuestosJuego[0].opuesto_im2;

      this.imageSrc1 = this.opuestos[aux2].url_im1;
      (<HTMLInputElement>document.getElementById('op1')).textContent = this.opuestos[aux2].opuesto_im1;

      this.imageSrc3 = this.opuestos[aux3].url_im2;
      (<HTMLInputElement>document.getElementById('op3')).textContent = this.opuestos[aux3].opuesto_im2;

      this.imageSrc4 = this.opuestos[aux4].url_im1;
      (<HTMLInputElement>document.getElementById('op4')).textContent = this.opuestos[aux4].opuesto_im1;

      this.imageSrc5 = this.opuestos[aux5].url_im2;
      (<HTMLInputElement>document.getElementById('op5')).textContent = this.opuestos[aux5].opuesto_im2;

      this.imageSrc6 = this.opuestos[aux6].url_im1;
      (<HTMLInputElement>document.getElementById('op6')).textContent = this.opuestos[aux6].opuesto_im1;

    } else if(this.aux == 3){
      this.imageSrc3 = this.opuestosJuego[0].url_im2;
      (<HTMLInputElement>document.getElementById('op3')).textContent = this.opuestosJuego[0].opuesto_im2;

      this.imageSrc1 = this.opuestos[aux2].url_im1;
      (<HTMLInputElement>document.getElementById('op1')).textContent = this.opuestos[aux2].opuesto_im1;

      this.imageSrc2 = this.opuestos[aux3].url_im2;
      (<HTMLInputElement>document.getElementById('op2')).textContent = this.opuestos[aux3].opuesto_im2;

      this.imageSrc4 = this.opuestos[aux4].url_im1;
      (<HTMLInputElement>document.getElementById('op4')).textContent = this.opuestos[aux4].opuesto_im1;

      this.imageSrc5 = this.opuestos[aux5].url_im1;
      (<HTMLInputElement>document.getElementById('op5')).textContent = this.opuestos[aux4].opuesto_im1;

      this.imageSrc6 = this.opuestos[aux6].url_im2;
      (<HTMLInputElement>document.getElementById('op6')).textContent = this.opuestos[aux4].opuesto_im2;

    } else if(this.aux == 4) {
      this.imageSrc4 = this.opuestosJuego[0].url_im2;
      (<HTMLInputElement>document.getElementById('op4')).textContent = this.opuestosJuego[0].opuesto_im2;

      this.imageSrc1 = this.opuestos[aux2].url_im1;
      (<HTMLInputElement>document.getElementById('op1')).textContent = this.opuestos[aux2].opuesto_im1;

      this.imageSrc2 = this.opuestos[aux3].url_im2;
      (<HTMLInputElement>document.getElementById('op2')).textContent = this.opuestos[aux3].opuesto_im2;

      this.imageSrc3 = this.opuestos[aux4].url_im1;
      (<HTMLInputElement>document.getElementById('op3')).textContent = this.opuestos[aux4].opuesto_im1;

      this.imageSrc5 = this.opuestos[aux5].url_im1;
      (<HTMLInputElement>document.getElementById('op5')).textContent = this.opuestos[aux5].opuesto_im1;

      this.imageSrc6 = this.opuestos[aux6].url_im2;
      (<HTMLInputElement>document.getElementById('op6')).textContent = this.opuestos[aux6].opuesto_im2;

    } else if(this.aux == 5){
      this.imageSrc5 = this.opuestosJuego[0].url_im2;
      (<HTMLInputElement>document.getElementById('op5')).textContent = this.opuestosJuego[0].opuesto_im2;

      this.imageSrc1 = this.opuestos[aux2].url_im1;
      (<HTMLInputElement>document.getElementById('op1')).textContent = this.opuestos[aux2].opuesto_im1;

      this.imageSrc2 = this.opuestos[aux3].url_im2;
      (<HTMLInputElement>document.getElementById('op2')).textContent = this.opuestos[aux3].opuesto_im2;

      this.imageSrc3 = this.opuestos[aux4].url_im1;
      (<HTMLInputElement>document.getElementById('op3')).textContent = this.opuestos[aux4].opuesto_im1;

      this.imageSrc4 = this.opuestos[aux5].url_im1;
      (<HTMLInputElement>document.getElementById('op4')).textContent = this.opuestos[aux5].opuesto_im1;

      this.imageSrc6 = this.opuestos[aux6].url_im2;
      (<HTMLInputElement>document.getElementById('op6')).textContent = this.opuestos[aux6].opuesto_im2;

    } else if(this.aux == 6){
      this.imageSrc6 = this.opuestosJuego[0].url_im2;
      (<HTMLInputElement>document.getElementById('op6')).textContent = this.opuestosJuego[0].opuesto_im2;

      this.imageSrc1 = this.opuestos[aux2].url_im1;
      (<HTMLInputElement>document.getElementById('op1')).textContent = this.opuestos[aux2].opuesto_im1;

      this.imageSrc2 = this.opuestos[aux3].url_im2;
      (<HTMLInputElement>document.getElementById('op2')).textContent = this.opuestos[aux3].opuesto_im2;

      this.imageSrc3 = this.opuestos[aux4].url_im1;
      (<HTMLInputElement>document.getElementById('op3')).textContent = this.opuestos[aux4].opuesto_im1;

      this.imageSrc4 = this.opuestos[aux5].url_im1;
      (<HTMLInputElement>document.getElementById('op4')).textContent = this.opuestos[aux5].opuesto_im1;

      this.imageSrc5 = this.opuestos[aux6].url_im2;
      (<HTMLInputElement>document.getElementById('op5')).textContent = this.opuestos[aux6].opuesto_im2;
    }
  }

  seleccionarIm0(){
    var h = new SpeechSynthesisUtterance();
    h.lang ="es-419";
    h.text = (<HTMLInputElement>document.getElementById('op0')).textContent;
    window.speechSynthesis.speak(h);
  }
  
  seleccionarIm1(){
    var h = new SpeechSynthesisUtterance();
    h.lang ="es-419";
    h.text = (<HTMLInputElement>document.getElementById('op1')).textContent;
    window.speechSynthesis.speak(h);
    if(this.aux == 1){
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'block';
      },2000);      
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
      }, 3000);
      //alert('Correcto');
    } else {
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'block';
      },2000);      
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
      }, 3000);
      //alert('Vuelve a intentar');
    }
  }
  
  seleccionarIm2(){
    var h = new SpeechSynthesisUtterance();
    h.lang ="es-419";
    h.text = (<HTMLInputElement>document.getElementById('op2')).textContent;
    window.speechSynthesis.speak(h);
    if(this.aux == 2){
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'block';
      },2000);      
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
      }, 3000);
      //alert('Correcto');
    } else {
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'block';
      },2000);      
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
      }, 3000);
      //alert('Vuelve a intentar');
    }
  }

  seleccionarIm3(){
    var h = new SpeechSynthesisUtterance();
    h.lang ="es-419";
    h.text = (<HTMLInputElement>document.getElementById('op3')).textContent;
    window.speechSynthesis.speak(h);
    if(this.aux == 3){
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'block';
      },2000);      
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
      }, 3000);
      //alert('Correcto');
    } else {
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'block';
      },2000);      
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
      }, 3000);
      //alert('Vuelve a intentar');
    }
  }

  seleccionarIm4(){
    var h = new SpeechSynthesisUtterance();
    h.lang ="es-419";
    h.text = (<HTMLInputElement>document.getElementById('op4')).textContent;
    window.speechSynthesis.speak(h);
    if(this.aux == 4){
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'block';
      },2000);      
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
      }, 3000);
      //alert('Correcto');
    } else {
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'block';
      },2000);      
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
      }, 3000);
      //alert('Vuelve a intentar');
    }
  }

  seleccionarIm5(){
    var h = new SpeechSynthesisUtterance();
    h.lang ="es-419";
    h.text = (<HTMLInputElement>document.getElementById('op5')).textContent;
    window.speechSynthesis.speak(h);
    if(this.aux == 5){
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'block';
      },2000);      
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
      }, 3000);
      //alert('Correcto');
    } else {
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'block';
      },2000);      
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
      }, 3000);
      //alert('Vuelve a intentar');
    }
  }

  seleccionarIm6(){
    var h = new SpeechSynthesisUtterance();
    h.lang ="es-419";
    h.text = (<HTMLInputElement>document.getElementById('op6')).textContent;
    window.speechSynthesis.speak(h);
    if(this.aux == 6){
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'block';
      },2000);      
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
      }, 3000);
      //alert('Correcto');
    } else {
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'block';
      },2000);      
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
      }, 3000);
      //alert('Vuelve a intentar');
    }
  }

  continuar(){
    if(this.opuestosJuego.length == 1){
      var audio = new Audio('../../../assets/img/good.mp3');
      audio.play();
      this.opuestosJuego.shift();
      (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
      (<HTMLInputElement>document.getElementById('last')).style.display = 'block';
      setTimeout(function(){
        localStorage.clear();
        var x = location.href='/home';
        document.getElementById('btn').innerHTML = x;
      }, 3000);
  
      //alert("Se acabo");
    } else {
      this.opuestosJuego.shift();
      this.inicioIm();
    }
  }

  randomIntFromInterval(min: number,max: number){
    return Math.floor(Math.random()*(max-min+1)+min);
  }



}
