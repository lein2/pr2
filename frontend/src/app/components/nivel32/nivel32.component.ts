import { Component, OnInit } from '@angular/core';
import {Opuesto} from '../../models/opuesto';
import { Event } from '@angular/router';

@Component({
  selector: 'app-nivel32',
  templateUrl: './nivel32.component.html',
  styleUrls: ['./nivel32.component.css']
})
export class Nivel32Component implements OnInit {
  opuestosJuego: Array<Opuesto>;
  opuestos: Array<Opuesto>;
  imageSrc: string;
  imageSrc1: string;
  aux1: number;
  aux2: number;
  aux3: number;
  aux4: number;
  validator: string = "";
  validator2: string = "";
  keyval: string = "";
  keyid: string = "";

  constructor() { }

  ngOnInit() {
    (<HTMLInputElement>document.getElementById('last')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
    (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';

    this.opuestosJuego = JSON.parse(localStorage.getItem('opuestos'));
    this.opuestos = JSON.parse(localStorage.getItem('opuestosfull'));
    console.log(this.opuestosJuego);
    console.log(this.opuestos);

    /*this.imageSrc = this.opuestosJuego[0].url_im1;
    this.imageSrc1 = this.opuestosJuego[0].url_im2;
    (<HTMLInputElement>document.getElementById('op1')).textContent = this.opuestosJuego[0].opuesto_im1;
    (<HTMLInputElement>document.getElementById('op2')).textContent = this.opuestosJuego[0].opuesto_im2;*/



    this.inicioIm();
  }

  inicioIm(){
    
    var b = false;
    var arraux = [];
    while(!b){
      var n = this.randomIntFromInterval(1,4);
      var find = arraux.find(function(num){
        return num == n;
      })
      if(!find){
        arraux.push(n);
      }
      if(arraux.length == 4){
        b = true;
      }
    }

    console.log(arraux);
    this.aux1 = arraux[0];
    this.aux2 = arraux[1];
    this.aux3 = arraux[2];
    this.aux4 = arraux[3];

    this.imageSrc = this.opuestosJuego[0].url_im1;
    this.imageSrc1 = this.opuestosJuego[0].url_im2;

    (<HTMLInputElement>document.getElementById('op1')).style.color = 'white';
    (<HTMLInputElement>document.getElementById('op2')).style.color = 'white';

    (<HTMLInputElement>document.getElementById('op1')).textContent = this.opuestosJuego[0].opuesto_im1;
    (<HTMLInputElement>document.getElementById('op2')).textContent = this.opuestosJuego[0].opuesto_im2;

    (<HTMLInputElement>document.getElementById(this.aux1.toString())).textContent = this.opuestosJuego[0].opuesto_im1;
    (<HTMLInputElement>document.getElementById(this.aux2.toString())).textContent = this.opuestosJuego[0].opuesto_im2;
    (<HTMLInputElement>document.getElementById(this.aux3.toString())).textContent = this.opuestos[this.randomIntFromInterval(0, this.opuestos.length-1)].opuesto_im2;
    (<HTMLInputElement>document.getElementById(this.aux4.toString())).textContent = this.opuestos[this.randomIntFromInterval(0, this.opuestos.length-1)].opuesto_im1;

    (<HTMLInputElement>document.getElementById('1')).style.display = 'block';
    (<HTMLInputElement>document.getElementById('2')).style.display = 'block';
    (<HTMLInputElement>document.getElementById('3')).style.display = 'block';
    (<HTMLInputElement>document.getElementById('4')).style.display = 'block';

  }

  evdragstart(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
    this.validator = ev.dataTransfer.getData("text", ev.target.id);
    this.validator2 =  (<HTMLInputElement>document.getElementById(this.validator)).textContent
    console.log(this.validator);
    console.log(this.validator2);
  }

  evdragover (ev) {
    ev.preventDefault();
  }

  evdrop(ev,el) {
    var test =  (<HTMLInputElement>document.getElementById(ev.target.id)).textContent
    //console.log('op' + ev.target.id)
    if(this.validator2 == test){
      ev.stopPropagation();
      ev.preventDefault();
      var data=ev.dataTransfer.getData("text");
      console.log(data);
      (<HTMLInputElement>document.getElementById(ev.target.id)).style.color = 'black';
      (<HTMLInputElement>document.getElementById(this.validator)).style.display = 'none';
      //(<HTMLInputElement>document.getElementById(ev.target.id)).textContent = '';
      //ev.target.appendChild(document.getElementById(data));
      var h = new SpeechSynthesisUtterance();
      h.lang ="es-419";
      h.text = (<HTMLInputElement>document.getElementById(ev.target.id)).textContent;
      window.speechSynthesis.speak(h);
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
      },2000);
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
      }, 3000);
      //alert('correcto');
    } else {
      ev.stopPropagation();
      ev.preventDefault();
      data=ev.dataTransfer.getData("text");
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'block';
      },500);
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
      }, 3000);
      //alert('Vuelve a intentar');
      //ev.target.appendChild(document.getElementById(data));
      //alert("MAL");
    }
       
  }

  /*allowDrop(ev){
    ev.preventDefault();
  }

  drag(ev){
    ev.dataTransfer.setData("text", ev.target.id);
  }

  drop(ev){
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
  }*/
  
  keyevent1(event){
    if (event.keyCode === 32) {
     //console.log(event);
     this.keyval = (<HTMLInputElement>document.getElementById("1")).textContent;
     (<HTMLInputElement>document.getElementById("1")).style.background = "#c2c2c2";
     (<HTMLInputElement>document.getElementById("2")).style.background = "#f1f1f1";
     (<HTMLInputElement>document.getElementById("3")).style.background = "#f1f1f1";
     (<HTMLInputElement>document.getElementById("4")).style.background = "#f1f1f1";
     this.keyid = "1";
     console.log(this.keyval);
    }
  }
  
  keyevent2(event){
    if (event.keyCode === 32) {
     //console.log(event);
     this.keyval = (<HTMLInputElement>document.getElementById("2")).textContent;
     (<HTMLInputElement>document.getElementById("1")).style.background = "#f1f1f1";
     (<HTMLInputElement>document.getElementById("2")).style.background = "#c2c2c2";
     (<HTMLInputElement>document.getElementById("3")).style.background = "#f1f1f1";
     (<HTMLInputElement>document.getElementById("4")).style.background = "#f1f1f1";
     this.keyid = "2";
     console.log(this.keyval);
    }
  }
  
  keyevent3(event){
    if (event.keyCode === 32) {
     //console.log(event);
     this.keyval = (<HTMLInputElement>document.getElementById("3")).textContent;
     (<HTMLInputElement>document.getElementById("1")).style.background = "#f1f1f1";
     (<HTMLInputElement>document.getElementById("2")).style.background = "#f1f1f1";
     (<HTMLInputElement>document.getElementById("3")).style.background = "#c2c2c2";
     (<HTMLInputElement>document.getElementById("4")).style.background = "#f1f1f1";
     this.keyid = "3";
     console.log(this.keyval);
    }
  }
  
  keyevent4(event){
    if (event.keyCode === 32) {
     //console.log(event);
     this.keyval = (<HTMLInputElement>document.getElementById("4")).textContent;
     (<HTMLInputElement>document.getElementById("1")).style.background = "#f1f1f1";
     (<HTMLInputElement>document.getElementById("2")).style.background = "#f1f1f1";
     (<HTMLInputElement>document.getElementById("3")).style.background = "#f1f1f1";
     (<HTMLInputElement>document.getElementById("4")).style.background = "#c2c2c2";
     this.keyid = "4";
     console.log(this.keyval);
    }
  }
  
  keyeventop1(event){
    if (event.keyCode === 32) {
     if(this.keyval == (<HTMLInputElement>document.getElementById("op1")).textContent)
     {
       console.log("Correcto");
       (<HTMLInputElement>document.getElementById("op1")).style.color = 'black';
       (<HTMLInputElement>document.getElementById(this.keyid)).style.display = 'none';
       //(<HTMLInputElement>document.getElementById(ev.target.id)).textContent = '';
       //ev.target.appendChild(document.getElementById(data));
       var h = new SpeechSynthesisUtterance();
       h.lang ="es-419";
       h.text = (<HTMLInputElement>document.getElementById(this.keyid)).textContent;
       window.speechSynthesis.speak(h);
       setTimeout(function(){
         (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
         (<HTMLInputElement>document.getElementById('correcto')).style.display = 'block';
         (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
       },2000);
       setTimeout(function(){
         (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
         (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
       }, 3000);

     }
     else {
       console.log("Incorrecto");
       setTimeout(function(){
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'block';
      },500);
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
      }, 3000);
     }
    }
  }
  
  keyeventop2(event){
    if (event.keyCode === 32) {
     if(this.keyval == (<HTMLInputElement>document.getElementById("op2")).textContent)
     {
       console.log("Correcto");
       (<HTMLInputElement>document.getElementById("op2")).style.color = 'black';
       (<HTMLInputElement>document.getElementById(this.keyid)).style.display = 'none';
       //(<HTMLInputElement>document.getElementById(ev.target.id)).textContent = '';
       //ev.target.appendChild(document.getElementById(data));
       var h = new SpeechSynthesisUtterance();
       h.lang ="es-419";
       h.text = (<HTMLInputElement>document.getElementById(this.keyid)).textContent;
       window.speechSynthesis.speak(h);
       setTimeout(function(){
         (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
         (<HTMLInputElement>document.getElementById('correcto')).style.display = 'block';
         (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
       },2000);
       setTimeout(function(){
         (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
         (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
       }, 3000);

     }
     else {
       console.log("Incorrecto");
       setTimeout(function(){
        (<HTMLInputElement>document.getElementById('correcto')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'block';
      },500);
      setTimeout(function(){
        (<HTMLInputElement>document.getElementById('container')).style.display = 'block';
        (<HTMLInputElement>document.getElementById('mal')).style.display = 'none';
      }, 3000);
     }
    }
  }

  continuar(){
    if(this.opuestosJuego.length == 1){
      var audio = new Audio('../../../assets/img/good.mp3');
      audio.play();
      this.opuestosJuego.shift();
      (<HTMLInputElement>document.getElementById('container')).style.display = 'none';
      (<HTMLInputElement>document.getElementById('last')).style.display = 'block';
      setTimeout(function(){
        localStorage.clear();
        var x = location.href='/home';
        document.getElementById('btn').innerHTML = x;
      }, 3000);
  
      //alert("Se acabo");
    } else {
      this.opuestosJuego.shift();
      this.inicioIm();
    }
  }

  randomIntFromInterval(min: number,max: number){
    return Math.floor(Math.random()*(max-min+1)+min);
  }

}
