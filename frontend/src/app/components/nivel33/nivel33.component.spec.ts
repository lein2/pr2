import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Nivel33Component } from './nivel33.component';

describe('Nivel33Component', () => {
  let component: Nivel33Component;
  let fixture: ComponentFixture<Nivel33Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Nivel33Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Nivel33Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
