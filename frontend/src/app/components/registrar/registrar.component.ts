import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsuarioService } from '../../services/usuario.service';
import {Usuario} from '../../models/usuario';

declare var M: any;

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css'],
  providers: [UsuarioService]
})
export class RegistrarComponent implements OnInit {

  constructor(private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.getUsuarios();
    //console.log(Usuario);
  }

  addUsuario(form: NgForm){
    var user = (<HTMLInputElement>document.getElementById('username')).value;
    var email = (<HTMLInputElement>document.getElementById('email')).value;
    var pass = (<HTMLInputElement>document.getElementById('password')).value;
    var repass = (<HTMLInputElement>document.getElementById('repassword')).value;
    
    this.getUsuarios();
    var Usuarios = this.usuarioService.usuarios;
    var findUser = Usuarios.find(function(Usuario){
      return Usuario.username === user;
    });

    var findEmail = Usuarios.find(function(Usuario){
      return Usuario.email === email;
    });

    if(findUser){
      M.toast({html: 'Usuario ya registrdo'});
    } else if(findEmail){
      M.toast({html: 'Un Usuario ya se registró con este E-Mail'});
    } else if(pass != repass){
      M.toast({html: 'No coinciden las contraseñas'});
    } else{
      this.usuarioService.postUsuario(form.value).subscribe(res => {
        this.resetForm(form);
        M.toast({html: 'Usuario Agreagado'});
        console.log('Usuario Agreagado');
        this.redirectLogin();
      });
    }
  }

  getUsuarios(){
    this.usuarioService.getUsuarios().subscribe(res =>{
      this.usuarioService.usuarios = res as Usuario[];
      //console.log(res);
    })
  }

  resetForm(form?: NgForm){
    if(form){
      form.reset;
      this.usuarioService.selectedUsuario = new Usuario();
    }
  }

  redirectLogin(){
    var x = location.href='/login';
    document.getElementById('boton').innerHTML = x;
  }

}
