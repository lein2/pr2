import { Usuario} from './usuario';
export class Opuesto {
    constructor(_id = '', url_im1 = '', url_im2 = '', nombre_im1 = '', nombre_im2 = '', opuesto_im1 = '', opuesto_im2 = '', usuario=''){
        this._id = _id;
        this.url_im1 = url_im1;
        this.url_im2 = url_im2;
        this.nombre_im1 = nombre_im1;
        this.nombre_im2 = nombre_im2;
        this.opuesto_im1 = opuesto_im1;
        this.opuesto_im2 = opuesto_im2;
        this.usuario = usuario;
    }
    _id: string;
    url_im1: string;
    url_im2: string;
    nombre_im1: string;
    nombre_im2: string;
    opuesto_im1: string;
    opuesto_im2: string;
    usuario: Object;
}
