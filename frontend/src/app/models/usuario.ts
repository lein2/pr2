export class Usuario {
    constructor(_id = '', username = '', password = '', email = '', opuestos = null){
        this._id = _id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.opuestos = opuestos;
    }
    _id: string;
    username: string;
    password: string;
    email: string;
    opuestos: Object[];
}
