import { TestBed, inject } from '@angular/core/testing';

import { NivelopuestosService } from './nivelopuestos.service';

describe('NivelopuestosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NivelopuestosService]
    });
  });

  it('should be created', inject([NivelopuestosService], (service: NivelopuestosService) => {
    expect(service).toBeTruthy();
  }));
});
