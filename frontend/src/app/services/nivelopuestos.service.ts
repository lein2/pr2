import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs-compat/BehaviorSubject';
import { Subject } from 'rxjs/Subject';

@Injectable()

export class NivelopuestosService {

  private datasource = new BehaviorSubject<Array<any>>([]);
  private datasource2 = new Subject<Array<any>>();
  observ = this.datasource.asObservable();
  observ2 = this.datasource.asObservable();

  constructor() { }

  setOpuestosNivel(opuestos: Array<any>){
    this.datasource.next(opuestos);
  }

  setOpuestosNivel2(opuestos: Array<any>){
    this.datasource.next(opuestos);
  }
  
}
