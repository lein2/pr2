import { TestBed, inject } from '@angular/core/testing';

import { OpuestoService } from './opuesto.service';

describe('OpuestoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OpuestoService]
    });
  });

  it('should be created', inject([OpuestoService], (service: OpuestoService) => {
    expect(service).toBeTruthy();
  }));
});
