import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Opuesto} from '../models/opuesto';

@Injectable({
  providedIn: 'root'
})
export class OpuestoService {

  selectedOpuesto: Opuesto;
  opuesto: Opuesto;
  opuestos: Opuesto[];
  readonly URL_APi = 'http://localhost:3000/api/opuestos';

  constructor(private http: HttpClient) { 
    this.selectedOpuesto = new Opuesto();
  }

  getOpuestos(){
    return this.http.get(this.URL_APi);
  }

  getOpuesto(_id: string){
    return this.http.get(this.URL_APi + `/${_id}`);
  }

  postOpuesto(opuesto: Opuesto){
    return this.http.post(this.URL_APi, opuesto);
  }

  putOpuesto(opuesto: Opuesto){
    return this.http.put(this.URL_APi + `/${opuesto._id}`, opuesto);
  }

  deleteOpuesto(_id: string){
    return this.http.delete(this.URL_APi + `/${_id}`);
  }

}
