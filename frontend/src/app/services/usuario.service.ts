import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Usuario} from '../models/usuario';
import { Opuesto } from '../models/opuesto';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  selectedUsuario: Usuario;
  selectedOpuesto: Opuesto;
  usuarios: Usuario[];
  opuestos: Opuesto[];
  readonly URL_APi = 'http://localhost:3000/api/usuarios';

  constructor(private http: HttpClient) { 
    this.selectedUsuario = new Usuario();
  }

  getUsuarios(){
    return this.http.get(this.URL_APi);
  }

  postUsuario(usuario: Usuario){
    return this.http.post(this.URL_APi, usuario);
  }

  putUsuario(usuario: Usuario){
    return this.http.put(this.URL_APi + `/${usuario._id}`, usuario);
  }

  deleteUsuario(_id: string){
    return this.http.delete(this.URL_APi + `/${_id}`);
  }

  getUsuario(_id: string){
    return this.http.get(this.URL_APi + `/${_id}`);
  }

  getOpuestosUsuario(_id: string){
    return this.http.get(this.URL_APi + `/${_id}/opuestos`)
  }
}
