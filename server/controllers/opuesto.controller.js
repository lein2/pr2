const Opuesto = require('../models/opuesto');
const Usuario = require('../models/usuario');
const fs = require('fs');
const opuestoCtrl = {};

opuestoCtrl.getOpuestos = async (req, res) =>{
    const opuestos = await Opuesto.find();
    res.json(opuestos);
};

opuestoCtrl.createOpuesto = async (req, res) =>{
    const opuesto = new Opuesto({
        url_im1: req.body.url_im1,
        url_im2: req.body.url_im2,
        nombre_im1: req.body.nombre_im1,
        nombre_im2: req.body.nombre_im2,
        opuesto_im1: req.body.opuesto_im1,
        opuesto_im2: req.body.opuesto_im2,
        usuario: req.body.usuario
    });
    const fullusuario = await Usuario.findById(req.body.usuario);
    delete opuesto.usuario;
    opuesto.usuario = fullusuario;
    await opuesto.save();

    fullusuario.opuestos.push(opuesto);
    await fullusuario.save();
    res.json({
        status: 'Saved Opuesto'
    });

}

opuestoCtrl.getOpuesto = async (req, res) =>{
    const { id } = req.params;
    const opuesto = await Opuesto.findById(id);
    res.json(opuesto);
};

opuestoCtrl.editOpuesto = async (req, res) =>{
    const { id } = req.params;
    const op = await Opuesto.findById(id);
    console.log('old opuesto' + op);
    const opuesto = {
        url_im1: req.body.url_im1,
        url_im2: req.body.url_im2,
        nombre_im1: req.body.nombre_im1,
        nombre_im2: req.body.nombre_im2,
        opuesto_im1: req.body.opuesto_im1,
        opuesto_im2: req.body.opuesto_im2
    } 
    /*console.log('new opuesto: {' + opuesto.url_im1 + '\n' + opuesto.url_im2
    + '\n' + opuesto.nombre_im1 + '\n' + opuesto.nombre_im2 + '\n' + 
    opuesto.opuesto_im1 + '\n' + opuesto.opuesto_im2 + '}');*/
    
    await Opuesto.findByIdAndUpdate(id, {$set: opuesto}, {new: true});
    res.json({
        status: 'Opuesto Updated'
    });

    if(opuesto.nombre_im1 != op.nombre_im1){
        fs.unlink('./images/upload/'+op.nombre_im1, (err) =>{
            if(err){
                console.log("No se pude borrar la imagen " + op.nombre_im1);
                tst = false;
            } else {
                console.log("Imagen borrada exitosamente: " + op.nombre_im1);
                tst = true;
            }
        });
    }
    if(opuesto.nombre_im2 != op.nombre_im2){
        fs.unlink('./images/upload/'+op.nombre_im2, (err) =>{
            if(err){
                console.log("No se pude borrar la imagen " + op.nombre_im2);
                tst = false;
            } else {
                console.log("Imagen borrada exitosamente: " + op.nombre_im2);
                tst = true;
            }
        });
    }
    
}

opuestoCtrl.deleteOpuesto = async (req, res) =>{
    const { id } = req.params;
    const op = await Opuesto.findById(id);
    const n1 = op.nombre_im1;
    const n2 = op.nombre_im2;
    var tst = false;
    fs.unlink('./images/upload/'+n1, (err) =>{
        if(err){
            console.log("No se pude borrar la imagen " + n1);
            tst = false;
        } else {
            console.log("Imagen borrada exitosamente: " + n1);
            tst = true;
        }
    });
    fs.unlink('./images/upload/'+n2, (err) =>{
        if(err){
            console.log("No se pude borrar la imagen " + n2);
            tst = false;
        } else {
            console.log("Imagen borrada exitosamente: " + n2);
            tst = true;
        }
    });
    
    await Opuesto.findByIdAndRemove(req.params.id);
    res.json({
        status: 'Opuesto Deleted'
    });
}

opuestoCtrl.getOpuestosUsuario = async (req, res) => {
    const { idUsuario } = req.params;
    const opuesto = await Opuesto.find(idUsuario);
    res.json(opuesto);
}
module.exports = opuestoCtrl;