const Usuario = require('../models/usuario');
const Opuesto = require('../models/opuesto');
const usuarioCtrl = {};

usuarioCtrl.getUsuarios = async (req, res) =>{
    const usuarios = await Usuario.find().populate('opuestos');
    res.json(usuarios);
};

usuarioCtrl.createUsuario = async (req, res, next) =>{
    const usuario = new Usuario({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    });
    await usuario.save();
    res.json({
        status: 'Saved Usuario'
    });

}

usuarioCtrl.getUsuario = async (req, res) =>{
    const { id } = req.params;
    const usuario = await Usuario.findById(id);
    res.json(usuario);
};

usuarioCtrl.editUsuario = async (req, res) =>{
    const { id } = req.params;
    const usuario = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    }
    await Usuario.findByIdAndUpdate(id, {$set: usuario}, {new: true});
    res.json({
        status: 'Usuario Updated'
    });
}

usuarioCtrl.deleteUsuario = async (req, res) =>{
    await Usuario.findByIdAndRemove(req.params.id);
    res.json({
        status: 'Usuario Deleted'
    });
}


usuarioCtrl.getUsuarioOpuestos = async (req, res) =>{
    const{ id } = req.params;
    const usuario = await Usuario.findById(id).populate('opuestos');
    res.json(usuario.opuestos);
}

usuarioCtrl.createUsuarioOpuestos = async (req, res) =>{
    const{ id } = req.params;
    
    const newopuesto = new Opuesto(req.body);

    //
    console.log(id);
    const usuario = await Usuario.findById(id);

    //console.log(usuario);

    newopuesto.usuario = usuario;
    
    await newopuesto.save();
    //console.log(newopuesto)
    usuario.opuestos.push(newopuesto);
    await usuario.save();
    res.json(newopuesto);
}
module.exports = usuarioCtrl;