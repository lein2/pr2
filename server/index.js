const express = require('express');
const morgan = require('morgan');
const app =  express();
const path = require('path');
const cors = require('cors');
const multer = require('multer');
const ejs = require('ejs');
const fs = require('fs');

const { mongoose } =  require('./database');

//Multer STORAGE
const storage = multer.diskStorage({
    destination:'./images/upload',
    filename: function(req, file, cb){
        cb(null, file.fieldname + '_' + Date.now() + path.extname(file.originalname));
    }

});

//INIT upload
const upload = multer({
    storage: storage,
    fileFilter: function(req, file, cb){
        checkFileType(file, cb)
    }
}).single('photo');

//Function FileType
function checkFileType(file, cb){
    //Allowed Extensions
    const filetypes = /jpeg|jpg|png|gif|bmp|raw/;
    //check Extension
    const extn = filetypes.test(path.extname(file.originalname).toLocaleLowerCase());
    //check Mime
    const mimetype = filetypes.test(file.mimetype);

    if(mimetype && extn){
        return cb(null, true);
    } else {
        cb('Error: Solo Imagenes');
    }
}

//settings
app.set('port', process.env.PORT || 3000);

//middleware
app.use(morgan('dev'));
app.use(express.json());
app.use(express.static('./images'));
app.use(cors({origin: 'http://localhost:4200'}));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods', 'POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.set('view engine', 'ejs');

//routes
app.use('/api/usuarios', require('./routes/usuario.routes'));
app.use('/api/opuestos', require('./routes/opuesto.routes'));

//image post
app.get('/', (req, res) => res.render('index'));


app.post('/upload', (req, res) => {
    //res.send('test');
    var imgpath = '';
    upload(req, res, (err) =>{
        if(err){
            res.json({
                msg: err
            });
        } else {
            if(req.file == undefined){
                res.json({
                    msg: 'Error: No se selecciono el archivo'
                });
            } else {
                imgpath = req.protocol + "://" + req.hostname + ':' + app.get('port')  + req.path + '/'+ req.file.filename;
                /*res.json({
                    msg: 'Archivo Subido Correctamente',
                    file: `uploads/${req.file.filename}`,
                    url: imgpath
                });*/
                
                console.log(imgpath);
                //return res.send(req.protocol + "://" + req.host + '/' + req.file.destination + '/'+ req.file.filename);
                //console.log(req.file.path);
                return res.send({
                    url: imgpath,
                    file: req.file.filename
                });
            }
        }
    });
});

//start the server
app.listen(app.get('port'), () =>{
    console.log('Server on port', app.get('port'));
});