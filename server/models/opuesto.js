const mongoose = require('mongoose');
const { Schema } = mongoose;

const OpuestosSchema = new Schema({
    url_im1: {type: String, required: true},
    url_im2: {type: String, required: true},
    nombre_im1: {type: String, required: true},
    nombre_im2: {type: String, required: true},
    opuesto_im1: {type: String, required: true},
    opuesto_im2: {type: String, required: true},
    usuario: {type: Schema.Types.ObjectId, ref: 'Usuario'}
});

module.exports = mongoose.model('Opuesto', OpuestosSchema);