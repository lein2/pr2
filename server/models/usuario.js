const mongoose = require('mongoose');
const { Schema } = mongoose;

const UsuariosSchema = new Schema({
    username: {type: String, required: true, unique: true},
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    opuestos: [{type: Schema.Types.ObjectId, ref: 'Opuesto'}]
});

module.exports = mongoose.model('Usuario', UsuariosSchema);