const express = require('express');
const router = express.Router();
const opuesto = require('../controllers/opuesto.controller');

router.get('/', opuesto.getOpuestos);
router.post('/', opuesto.createOpuesto);
router.get('/:id', opuesto.getOpuesto);
router.put('/:id', opuesto.editOpuesto);
router.delete('/:id', opuesto.deleteOpuesto);
router.get('/:idUsuario', opuesto.getOpuestosUsuario);

module.exports = router;